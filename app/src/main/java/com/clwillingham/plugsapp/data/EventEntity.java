package com.clwillingham.plugsapp.data;

import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

@ParseClassName("Event")
public class EventEntity extends ParseObject {
    public static final String TYPE = "type";
    public static final String TRIGGER = "trigger";
    public static final String DEVICE = "device";
    public static final String DEVICE_ID = "device_id";
    public static final String DATA = "data";
    public static final String IMAGE = "image";

    public EventEntity(){
//        setTriggers(new ArrayList<TriggerEntity>());
    }

    public void fire(TriggerEntity trigger) throws ParseException {
        List<ActionEntity> actions = trigger.getActions();
        if(actions == null) return; //if there are no actions, then there is nothing to do.
        for(ActionEntity action : actions){
            CommandEntity cmd = new CommandEntity(this, action, null); //TODO: handle args
            cmd.saveInBackground();
        }
    }

    public void setType(String type){
        put(TYPE, type);
    }
    public String getType(){
        return getString(TYPE);
    }

    public void setData(JSONObject data){
        put(DATA, data);
    }
    public JSONObject getData(){
        return getJSONObject(DATA);
    }

    public ParseFile getImage(){
        return getParseFile(IMAGE);
    }

    public void setImage(ParseFile file){
        put(IMAGE, file);
    }

//    public void setTriggers(List<TriggerEntity> triggers){
//        put(TRIGGERS, triggers);
//    }
//    public List<TriggerEntity> getTriggers(){
//        return getList(TRIGGERS);
//    }
    public void setTrigger(TriggerEntity trigger){
        put(TRIGGER, trigger);
        put(DEVICE, trigger.getDevice());
        put(DEVICE_ID, trigger.getDevice().getObjectId());
    }
}
