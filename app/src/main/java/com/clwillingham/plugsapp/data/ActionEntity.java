package com.clwillingham.plugsapp.data;

import android.util.Log;

import com.clwillingham.plugsapp.App;
import com.clwillingham.plugsapp.actions.Action;
import com.clwillingham.plugsapp.events.data.actions.ActionUpdatedEvent;
import com.parse.GetCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.livequery.SubscriptionHandling;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

@ParseClassName("Action")
public class ActionEntity extends ParseObject {
    private static final String TAG = "ActionEntity";

    public static final String DEVICE = "device";
    public static final String DEVICE_ID = "device_id";
    public static final String TYPE = "type";
    public static final String ENABLED = "enabled";
    public static final String ARG_TYPES = "arg_types";

    public ActionEntity(){
        super();
    }

    public ActionEntity(DeviceEntity device, Action action){
        setType(action.getName());
        setDevice(device);
    }

    public static ParseQuery<ActionEntity> query(DeviceEntity deviceEntity, String type){
        return new ParseQuery<>(ActionEntity.class)
                .whereEqualTo(DEVICE, deviceEntity)
                .whereEqualTo(TYPE,   type);
    }

    public void find(final GetCallback<ActionEntity> cb){
        query(getDevice(), getType()).getFirstInBackground(new GetCallback<ActionEntity>() {
            @Override
            public void done(ActionEntity object, ParseException e) {
                if(e != null){
                    e.printStackTrace();
                    cb.done(object, e);
                    return;
                }
                setObjectId(object.getObjectId());
                fetchIfNeededInBackground(cb);
            }
        });
    }

    public DeviceEntity getDevice() {
        return (DeviceEntity) get(DEVICE);
    }
    public void setDevice(DeviceEntity device) {
        put(DEVICE, device);
        put(DEVICE_ID, device.getObjectId());
    }

    public void setType(String type){
        put(TYPE, type);
    }
    public String getType(){
        return getString(TYPE);
    }

    public void setEnabled(boolean enabled){
        put(ENABLED, enabled);
    }

    public static ParseQuery<ActionEntity> query(DeviceEntity device){
        return new ParseQuery<>(ActionEntity.class).whereEqualTo(DEVICE, device);
    }

    public static void subscribe(DeviceEntity device){
        SubscriptionHandling<ActionEntity> subscriptionHandler = App.getParseClient().subscribe(query(device));
        subscriptionHandler.handleEvents(new SubscriptionHandling.HandleEventsCallback<ActionEntity>() {
            @Override
            public void onEvents(ParseQuery<ActionEntity> query, SubscriptionHandling.Event event, ActionEntity object) {
                Log.d(TAG, "onEvents: " + event + " " + object.getType());
                switch (event){
                    case UPDATE:
                        EventBus.getDefault().post(new ActionUpdatedEvent(object));
                }
            }
        });
    }

    public static void unsubscribe(DeviceEntity device){
        App.getParseClient().unsubscribe(query(device));
    }

    public boolean getEnabled(){
        return getBoolean(ENABLED);
    }

    public void setArgTypes(JSONObject arg_types){
        put(ARG_TYPES, arg_types);
    }
}
