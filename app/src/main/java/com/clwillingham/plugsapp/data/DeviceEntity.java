package com.clwillingham.plugsapp.data;

import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.clwillingham.plugsapp.App;
import com.clwillingham.plugsapp.actions.Action;
import com.clwillingham.plugsapp.events.data.commands.CommandCreatedEvent;
import com.clwillingham.plugsapp.events.data.devices.DeviceDeletedEvent;
import com.clwillingham.plugsapp.events.data.devices.DeviceUpdatedEvent;
import com.clwillingham.plugsapp.events.data.sensors.SensorUpdatedEvent;
import com.clwillingham.plugsapp.triggers.Trigger;
import com.clwillingham.plugsapp.triggers.annotations.Arg;
import com.clwillingham.plugsapp.triggers.annotations.Args;
import com.clwillingham.plugsapp.triggers.annotations.TriggerName;
import com.parse.GetCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.livequery.SubscriptionHandling;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

@ParseClassName("Device")
public class DeviceEntity extends ParseObject {
    private static final String TAG = "DeviceEntity";
    public static final String NAME = "name";
    public static final String SENSORS = "sensors";
    public static final String ACTIONS = "actions";
    public static final String TYPE = "type";
    public static final String TRACK_LOCATION = "track_location";
    public static final String TRIGGER_TYPES = "trigger_types";
    private static DeviceEntity deviceEntity;

    private boolean subscribed = false;

    public DeviceEntity(String deviceId){
        this();
        put(TRIGGER_TYPES, new JSONArray());
        if(deviceId != null){
            setObjectId(deviceId);
        }else{
            setTrackLocation(true);
        }
    }

    public DeviceEntity(){

    }

    public String getName() {
        return getString(NAME);
    }

    public ParseQuery<SensorEntity> getSensors() {
        return ParseQuery.getQuery(SensorEntity.class).whereEqualTo(SensorEntity.DEVICE, this);
    }

    public static void getInstance(App app, @NonNull GetCallback<DeviceEntity> cb) {
        if (deviceEntity != null) {
            cb.done(deviceEntity, null);
            return;
        }
        String deviceId = app.getPrefs().getString(App.PREF_DEVICE_ID, null);
        deviceEntity = new DeviceEntity(deviceId);
        if (deviceId != null) {
            deviceEntity.fetchInBackground(cb);
        } else {
            cb.done(deviceEntity, null);
        }
    }

    public static void fetchInBackground(final App app) {
        if (deviceEntity != null) {
            EventBus.getDefault().postSticky(deviceEntity);
            return;
        }
        String deviceId = app.getPrefs().getString(App.PREF_DEVICE_ID, null);
        deviceEntity = new DeviceEntity(deviceId);
        if (deviceId != null) {
            deviceEntity.setObjectId(deviceId);
            deviceEntity.fetchInBackground(new GetCallback<DeviceEntity>() {
                @Override
                public void done(DeviceEntity object, ParseException e) {
                    if (e != null) {
                        EventBus.getDefault().post(e);
                        e.printStackTrace();
                        if(e.getCode() == ParseException.OBJECT_NOT_FOUND){
                            app.getPrefs().edit().remove(App.PREF_DEVICE_ID).apply();
                            deviceEntity = null;
                            fetchInBackground(app);

                        }
                    }else{
                        deviceEntity = object;
                        EventBus.getDefault().postSticky(deviceEntity);
                    }

                }
            });
        } else {
            EventBus.getDefault().postSticky(deviceEntity);
        }
    }

    private ParseQuery<DeviceEntity> selfQuery(){
        return ParseQuery.getQuery(DeviceEntity.class).whereEqualTo("objectId", getObjectId());
    }

    public void subscribe(){
        ParseQuery<DeviceEntity> query = selfQuery();
        SubscriptionHandling<DeviceEntity> subscriptionHandler = App.getParseClient().subscribe(query);
        subscriptionHandler.handleEvents(new SubscriptionHandling.HandleEventsCallback<DeviceEntity>() {
            @Override
            public void onEvents(ParseQuery<DeviceEntity> query, SubscriptionHandling.Event event, DeviceEntity object) {
                switch (event){
                    case ENTER:
                    case CREATE:
                        //TODO: maybe something more useful here
                        Log.d(TAG, "onEvents: WTF this shouldn't happen");
                        break;
                    case UPDATE:
                        EventBus.getDefault().post(new DeviceUpdatedEvent(object));
                        //TODO: figure out how the device should really be represented in EventBus
//                        EventBus.getDefault().postSticky(object);
                        deviceEntity = object;
                        break;
                    case LEAVE:
                    case DELETE:
                        deviceEntity = null;
                        EventBus.getDefault().post(new DeviceDeletedEvent(object));
                        break;
                }
            }
        });
    }

    public ParseQuery<CommandEntity> getCommands(){ //TODO: write query method in CommandEntity class
        return new ParseQuery<>(CommandEntity.class).whereEqualTo(CommandEntity.DEVICE, this);
    }

    public void subscribeToCommands(){ //TODO: move code to CommandEntity class
        SubscriptionHandling<CommandEntity> commandSubscriptionHandler = App.getParseClient().subscribe(getCommands());
        commandSubscriptionHandler.handleEvents(new SubscriptionHandling.HandleEventsCallback<CommandEntity>() {
            @Override
            public void onEvents(ParseQuery<CommandEntity> query, SubscriptionHandling.Event event, CommandEntity object) {
//                Log.d(TAG, "onEvents: " + event + " " + object.getAction().getType());

                switch (event){
                    case ENTER:
                    case CREATE:
                        EventBus.getDefault().post(new CommandCreatedEvent(object));
                }
            }
        });
    }

    public void subscribeToSensors(){ //TODO: move code to SensorEntity class
        SubscriptionHandling<SensorEntity> sensorSubscriptionHandler = App.getParseClient().subscribe(getSensors());
        sensorSubscriptionHandler.handleEvents(new SubscriptionHandling.HandleEventsCallback<SensorEntity>() {
            @Override
            public void onEvents(ParseQuery<SensorEntity> query, SubscriptionHandling.Event event, SensorEntity object) {
                Log.d(TAG, "onEvents: " + event + " " + object.getName());
                switch (event){
                    case UPDATE:
                        EventBus.getDefault().post(new SensorUpdatedEvent(object));
                }
            }
        });
    }

    public void subscribeToActions(){
        ActionEntity.subscribe(this);
    }
    public void subscribeToTriggers(){
        TriggerEntity.subscribe(this);
    }

    public void subscribeAll(){
        subscribe();
        subscribeToActions();
        subscribeToCommands();
        subscribeToSensors();
        subscribeToTriggers();
    }

    public void unsubscribe(){
        App.getParseClient().unsubscribe(selfQuery());
        App.getParseClient().unsubscribe(getSensors());
        App.getParseClient().unsubscribe(getCommands()); //TODO: add unsubscribe methods to respective entity classes
        ActionEntity.unsubscribe(this);
        TriggerEntity.unsubscribe(this);
    }

    public void setAsDevice(SharedPreferences prefs) {
        prefs
                .edit()
                .putString(App.PREF_DEVICE_ID, getObjectId())
                .apply();
    }

    public void setType(String type) {
        put(TYPE, type);
    }

    public String getType() {
        return getString(TYPE);
    }

    public void setName(String name) {
        put(NAME, name);
    }

    public ParseQuery<ActionEntity> getActions(){
        return new ParseQuery<>(ActionEntity.class).whereEqualTo(ActionEntity.DEVICE, this);
    }

    public boolean isTrackingLocation(){
        return getBoolean(TRACK_LOCATION);
    }

    public void setTrackLocation(boolean value){
        put(TRACK_LOCATION, value);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void addAction(final Action action){
        final ActionEntity actionEntity = new ActionEntity(this, action);
        ActionEntity.query(this, action.getName()).getFirstInBackground(new GetCallback<ActionEntity>() {
            @Override
            public void done(ActionEntity object, ParseException e) {
                if(e != null && e.getCode() == ParseException.OBJECT_NOT_FOUND){
                    try {
                        actionEntity.save();
                    } catch (ParseException e1) {
                        e1.printStackTrace();
                    }
                }else if(e != null){
                    e.printStackTrace();
                }
            }
        });
    }

    public JSONArray getTriggerTypes(){
        return getJSONArray(TRIGGER_TYPES);
    }

    public<T extends Trigger> void addTriggerType(Class<T> cls) throws JSONException {
        JSONObject triggerType = new JSONObject();
        String name = cls.getAnnotation(TriggerName.class).value();
        Args args = cls.getAnnotation(Args.class);
        JSONObject jsonArgs = new JSONObject();
        for(Arg arg : args.value()){
            jsonArgs.put(arg.key(), arg.type());
        }

        triggerType.put("name", name);
        triggerType.put("args", jsonArgs);
        JSONArray triggerTypes = getTriggerTypes();
        triggerTypes.put(triggerType);
        put(TRIGGER_TYPES, triggerTypes);
    }

    /**
     * create a new sensor and save in base station if it doesn't already exist. if it does exist return it;
     *
     * @param sensor device sensor to locate in database or add to database
     * @return the new or existing sensor after it has been fetched or saved to database.
     * @throws ParseException
     */
    public SensorEntity maybeNewSensor(Sensor sensor) throws ParseException {
        return SensorEntity.getOrCreate(this, sensor);
    }


    public SensorEntity getSensor(Sensor sensor) throws ParseException {
        return SensorEntity.getSensor(this, sensor);
    }

    public static DeviceEntity getInstance(App app) throws ParseException {
        if (deviceEntity != null) {
            return deviceEntity;
        }
        String deviceId = app.getPrefs().getString(App.PREF_DEVICE_ID, null);
        deviceEntity = new DeviceEntity(deviceId);
        if (deviceId != null) {
            deviceEntity.setObjectId(deviceId);
            return deviceEntity.fetch();
        }
        return deviceEntity;
    }

    public ParseQuery<TriggerEntity> getTriggers(){
        return TriggerEntity.query(this);
    }
}
