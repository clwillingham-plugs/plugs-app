package com.clwillingham.plugsapp.data;

import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

@ParseClassName("Command")
public class CommandEntity extends ParseObject {
    public static final String DEVICE = "device";
    public static final String ACTION = "action";
    public static final String EVENT = "event";
    public static final String ARGS   = "args";

    public CommandEntity(){

    }

    public CommandEntity(EventEntity eventEntity, ActionEntity action, JSONObject args) throws ParseException {
        setEvent(eventEntity);
        setAction(action);
        action.fetchIfNeeded();
        setDevice(action.getDevice());
//        setArgs(args);
    }

    public DeviceEntity getDevice() {
        return (DeviceEntity) get(DEVICE);
    }
    public void setDevice(DeviceEntity device) {
        put(DEVICE, device);
    }

    public ActionEntity getAction(){
        return (ActionEntity) get(ACTION);
    }
    public void setAction(ActionEntity action){
        put(ACTION, action);
    }

    public void setEvent(EventEntity event){
        put(EVENT, event);
    }
    public EventEntity getEvent(){
        return (EventEntity) get(EVENT);
    }

    public void setArgs(JSONObject args){
        put(ARGS, args);
    }
    public JSONObject getArgs(){
        return getJSONObject(ARGS);
    }
}
