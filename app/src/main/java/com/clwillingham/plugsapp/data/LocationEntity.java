package com.clwillingham.plugsapp.data;

import android.location.Location;
import android.os.Build;

import com.parse.ParseClassName;
import com.parse.ParseObject;

@ParseClassName("Location")
public class LocationEntity extends ParseObject {
    public static final String DEVICE = "device";
    public static final String LAT = "lat";
    public static final String LNG = "lng";
    public static final String ACCURACY = "accuracy";
    public static final String BEARING = "bearing";
    public static final String BEARING_ACCURACY = "bearing_accuracy";

    public LocationEntity(){

    }

    public LocationEntity(DeviceEntity deviceEntity, Location location){
        setDevice(deviceEntity);
        setLocation(location);
    }

    public void setLocation(Location location){
        setLat(location.getLatitude());
        setLng(location.getLongitude());
        //TODO: include timestamp, figure out reference time for location.getTime()
        setAccuracy(location.getAccuracy());
        setBearing(location.getBearing());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            setBearingAccuracy(location.getBearingAccuracyDegrees());
        }
    }

    public DeviceEntity getDevice() {
        return (DeviceEntity) get(DEVICE);
    }
    public void setDevice(DeviceEntity device) {
        put(DEVICE, device);
    }

    public void setLat(double lat){
        put(LAT, lat);
    }
    public double getLat(){
        return (float) getDouble(LAT);
    }

    public void setLng(double lng){
        put(LNG, lng);
    }
    public double getLng(){
        return (float) getDouble(LNG);
    }

    public void setAccuracy(float accuracy){
        put(ACCURACY, accuracy);
    }
    public float getAccuracy(){
        return (float) getDouble(ACCURACY);
    }

    public void setBearing(float bearing){
        put(BEARING, bearing);
    }
    public float getBearing(){
        return (float) getDouble(BEARING);
    }

    public void setBearingAccuracy(float bearingAccuracy){
        put(BEARING_ACCURACY, bearingAccuracy);
    }
    public float getBearingAccuracy(){
        return (float) getDouble(BEARING_ACCURACY);
    }
}
