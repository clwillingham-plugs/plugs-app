package com.clwillingham.plugsapp.data;

import android.hardware.Sensor;
import android.hardware.SensorManager;

import com.clwillingham.plugsapp.util.SensorUtils;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.livequery.SubscriptionHandling;

@ParseClassName("Sensor")
public class SensorEntity extends ParseObject {
    public static final String TYPE = "type";
    public static final String SAMPLE_RATE = "sample_rate";
    public static final String NAME = "name";
    public static final String DEVICE = "device";
    public static final String ENABLED = "enabled";

    public SensorEntity() {
        super();
    }

    public SensorEntity(String name, String type, int sampleRate, DeviceEntity device) {
        setName(name);
        setType(type);
        setDevice(device);
        setSampleRate(sampleRate);
        setEnabled(false);
    }

    public SensorEntity(DeviceEntity device, Sensor sensor){
        this(
                sensor.getName(),
                SensorUtils.getStringType(sensor.getType()),
                SensorManager.SENSOR_DELAY_UI,
                device
        );
    }

    private static ParseQuery<SensorEntity> querySensor(DeviceEntity device, Sensor sensor){
        return new ParseQuery<>(SensorEntity.class)
                .whereEqualTo(DEVICE, device)
                .whereEqualTo(TYPE, SensorUtils.getStringType(sensor));
    }

    public static SensorEntity getOrCreate(DeviceEntity device, Sensor sensor) throws ParseException {
        try{
            return querySensor(device, sensor).getFirst();
        }catch (ParseException e){
            if(e.getCode() == ParseException.OBJECT_NOT_FOUND){
                SensorEntity sensorEntity = new SensorEntity(device, sensor);
                sensorEntity.save();
                return sensorEntity;
            }else throw e;
        }
    }

    public static SensorEntity getSensor(DeviceEntity device, Sensor sensor) throws ParseException {
        try{
            return querySensor(device, sensor).getFirst();
        }catch(ParseException e){
            if(e.getCode() == ParseException.OBJECT_NOT_FOUND){
                return null;
            }else throw e;
        }
    }

    public String getName() {
        return getString(NAME);
    }

    public void setName(String name) {
        put(NAME, name);
    }

    public String getType() {
        return getString(TYPE);
    }

    public int getIntType(){
        return SensorUtils.getIntType(getType());
    }

    public void setType(String type) {
        put(TYPE, type);
    }

    public int getSampleRate() {
        return getInt(SAMPLE_RATE);
    }

    public boolean isEnabled(){
        return getBoolean(ENABLED);
    }

    public void setEnabled(boolean enabled){
        put(ENABLED, enabled);
    }

    public void setSampleRate(int sampleRate) {
        put(SAMPLE_RATE, sampleRate);
    }

    public DeviceEntity getDevice() {
        return (DeviceEntity) get(DEVICE);
    }

    public void setDevice(DeviceEntity device) {
        put(DEVICE, device);
    }


}
