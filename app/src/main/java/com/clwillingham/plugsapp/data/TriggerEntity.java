package com.clwillingham.plugsapp.data;

import android.util.Log;

import com.clwillingham.plugsapp.App;
import com.clwillingham.plugsapp.events.data.actions.ActionUpdatedEvent;
import com.clwillingham.plugsapp.events.data.triggers.TriggerCreatedEvent;
import com.clwillingham.plugsapp.events.data.triggers.TriggerDeletedEvent;
import com.clwillingham.plugsapp.events.data.triggers.TriggerUpdatedEvent;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.livequery.SubscriptionHandling;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

@ParseClassName("Trigger")
public class TriggerEntity extends ParseObject {
    private static final String TAG = "TriggerEntity";

    public static final String TYPE = "type";
    public static final String DEVICE = "device";
    public static final String ACTIONS = "actions";
    public static final String ACTION_IDS = "action_ids";
    public static final String ENABLED = "enabled";
    public static final String ARGS = "args";

    public void setType(String type){
        put(TYPE, type);
    }
    public String getType(){
        return getString(TYPE);
    }

    public void setDevice(DeviceEntity device){
        put(DEVICE, device);
    }
    public DeviceEntity getDevice(){
        return (DeviceEntity) get(DEVICE);
    }

    public void setEnabled(boolean enabled){
        put(ENABLED, enabled);
    }

    public JSONObject getArgs(){
        return getJSONObject(ARGS);
    }
    public void setArgs(JSONObject args){
        put(ARGS, args);
    }



    public void setActions(List<ActionEntity> actions){
        put(ACTIONS, actions);
        ArrayList<String> ids = new ArrayList<>();
        for(ActionEntity action : actions){
            ids.add(action.getObjectId());
        }
        put(ACTION_IDS, ids);
    }
    public List<ActionEntity> getActions(){
        return getList(ACTIONS);
    }

    public void addAction(ActionEntity action){
        add(ACTIONS, action);
        add(ACTION_IDS, action.getObjectId());
    }

    public boolean isEnabled(){
        return getBoolean(ENABLED);
    }

    public static ParseQuery<TriggerEntity> query(DeviceEntity device){
        return new ParseQuery<>(TriggerEntity.class).whereEqualTo(DEVICE, device);
    }

    public static void subscribe(DeviceEntity device){
        SubscriptionHandling<TriggerEntity> subscriptionHandler = App.getParseClient().subscribe(query(device));
        subscriptionHandler.handleEvents(new SubscriptionHandling.HandleEventsCallback<TriggerEntity>() {
            @Override
            public void onEvents(ParseQuery<TriggerEntity> query, SubscriptionHandling.Event event, TriggerEntity object) {
                switch (event){
                    case ENTER:
                    case CREATE:
                        EventBus.getDefault().post(new TriggerCreatedEvent(object));
                        break;
                    case LEAVE:
                    case DELETE:
                        EventBus.getDefault().post(new TriggerDeletedEvent(object));
                        break;
                    case UPDATE:
                        EventBus.getDefault().post(new TriggerUpdatedEvent(object));
                }
            }
        });
    }

    public static void unsubscribe(DeviceEntity device){
        App.getParseClient().unsubscribe(query(device));
    }

}
