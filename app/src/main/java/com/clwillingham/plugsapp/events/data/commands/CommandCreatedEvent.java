package com.clwillingham.plugsapp.events.data.commands;

import com.clwillingham.plugsapp.data.CommandEntity;

public class CommandCreatedEvent extends CommandEvent {
    public CommandCreatedEvent(CommandEntity command) {
        super(command);
    }
}
