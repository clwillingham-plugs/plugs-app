package com.clwillingham.plugsapp.events.data.triggers;


import com.clwillingham.plugsapp.data.TriggerEntity;

public class TriggerCreatedEvent extends TriggerEvent {
    public TriggerCreatedEvent(TriggerEntity trigger) {
        super(trigger);
    }
}
