package com.clwillingham.plugsapp.events;

import android.hardware.Sensor;

/**
 * Created by chris on 4/26/16.
 */
public class SensorCalibratedEvent {
    public final Sensor sensor;
    public final long offset;
    public final long divisor;

    public SensorCalibratedEvent(Sensor sensor, long offset, long divisor) {
        this.sensor = sensor;
        this.offset = offset;
        this.divisor = divisor;
    }
}
