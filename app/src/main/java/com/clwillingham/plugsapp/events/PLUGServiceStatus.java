package com.clwillingham.plugsapp.events;

public class PLUGServiceStatus {
    final boolean running;

    public PLUGServiceStatus(boolean running) {
        this.running = running;
    }

    public boolean isRunning() {
        return running;
    }
}
