package com.clwillingham.plugsapp.events.commands;

import com.clwillingham.plugsapp.data.EventEntity;

public class TakePictureCommand {
    private final EventEntity event;

    public TakePictureCommand(EventEntity event) {
        this.event = event;
    }

    public EventEntity getEvent() {
        return event;
    }
}
