package com.clwillingham.plugsapp.events.data.triggers;

import com.clwillingham.plugsapp.data.TriggerEntity;

public class TriggerEvent {
    public final TriggerEntity trigger;

    public TriggerEvent(TriggerEntity trigger) {
        this.trigger = trigger;
    }
}
