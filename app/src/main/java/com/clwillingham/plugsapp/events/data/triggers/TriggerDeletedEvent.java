package com.clwillingham.plugsapp.events.data.triggers;


import com.clwillingham.plugsapp.data.TriggerEntity;

public class TriggerDeletedEvent extends TriggerEvent {
    public TriggerDeletedEvent(TriggerEntity trigger) {
        super(trigger);
    }
}
