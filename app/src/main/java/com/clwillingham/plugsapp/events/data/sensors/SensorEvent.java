package com.clwillingham.plugsapp.events.data.sensors;

import com.clwillingham.plugsapp.data.SensorEntity;

public class SensorEvent {
    private final SensorEntity sensor;

    public SensorEvent(SensorEntity sensor) {
        this.sensor = sensor;
    }

    public SensorEntity getSensor() {
        return sensor;
    }
}
