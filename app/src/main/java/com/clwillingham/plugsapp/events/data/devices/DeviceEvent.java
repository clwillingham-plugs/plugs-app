package com.clwillingham.plugsapp.events.data.devices;

import com.clwillingham.plugsapp.data.DeviceEntity;

public class DeviceEvent {
    private final DeviceEntity device;

    public DeviceEvent(DeviceEntity device) {
        this.device = device;
    }

    public DeviceEntity getDevice() {
        return device;
    }
}
