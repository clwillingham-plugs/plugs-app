package com.clwillingham.plugsapp.events.data.triggers;


import com.clwillingham.plugsapp.data.TriggerEntity;

public class TriggerUpdatedEvent extends TriggerEvent {
    public TriggerUpdatedEvent(TriggerEntity trigger) {
        super(trigger);
    }
}
