package com.clwillingham.plugsapp.events.data.devices;

import com.clwillingham.plugsapp.data.DeviceEntity;

public class DeviceUpdatedEvent extends DeviceEvent{
    public DeviceUpdatedEvent(DeviceEntity device) {
        super(device);
    }
}
