package com.clwillingham.plugsapp.events.data.sensors;

import com.clwillingham.plugsapp.data.SensorEntity;

public class SensorUpdatedEvent extends SensorEvent{
    public SensorUpdatedEvent(SensorEntity sensor) {
        super(sensor);
    }
}
