package com.clwillingham.plugsapp.events.data.commands;

import com.clwillingham.plugsapp.data.CommandEntity;

public class CommandEvent {
    private final CommandEntity command;

    public CommandEvent(CommandEntity command) {
        this.command = command;
    }

    public CommandEntity getCommand() {
        return command;
    }
}
