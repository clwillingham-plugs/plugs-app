package com.clwillingham.plugsapp.events.data.actions;

import com.clwillingham.plugsapp.data.ActionEntity;

public class ActionEvent {
    private final ActionEntity action;

    public ActionEvent(ActionEntity action) {
        this.action = action;
    }

    public ActionEntity getAction() {
        return action;
    }
}
