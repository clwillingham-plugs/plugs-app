package com.clwillingham.plugsapp.events.data.devices;

import com.clwillingham.plugsapp.data.DeviceEntity;

public class DeviceDeletedEvent extends DeviceEvent {
    public DeviceDeletedEvent(DeviceEntity device){
        super(device);
    }
}
