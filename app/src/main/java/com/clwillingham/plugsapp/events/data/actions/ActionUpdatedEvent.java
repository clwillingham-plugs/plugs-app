package com.clwillingham.plugsapp.events.data.actions;

import com.clwillingham.plugsapp.data.ActionEntity;

public class ActionUpdatedEvent extends ActionEvent {

    public ActionUpdatedEvent(ActionEntity action) {
        super(action);
    }
}
