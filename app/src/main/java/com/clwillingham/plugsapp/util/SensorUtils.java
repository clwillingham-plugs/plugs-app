package com.clwillingham.plugsapp.util;

import android.hardware.Sensor;

import static android.hardware.Sensor.STRING_TYPE_LINEAR_ACCELERATION;
import static android.hardware.Sensor.TYPE_ACCELEROMETER;
import static android.hardware.Sensor.TYPE_AMBIENT_TEMPERATURE;
import static android.hardware.Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR;
import static android.hardware.Sensor.TYPE_GRAVITY;
import static android.hardware.Sensor.TYPE_GYROSCOPE;
import static android.hardware.Sensor.TYPE_GYROSCOPE_UNCALIBRATED;
import static android.hardware.Sensor.TYPE_LIGHT;
import static android.hardware.Sensor.TYPE_LINEAR_ACCELERATION;
import static android.hardware.Sensor.TYPE_MAGNETIC_FIELD;
import static android.hardware.Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED;
import static android.hardware.Sensor.TYPE_ORIENTATION;
import static android.hardware.Sensor.TYPE_PRESSURE;
import static android.hardware.Sensor.TYPE_PROXIMITY;
import static android.hardware.Sensor.TYPE_RELATIVE_HUMIDITY;
import static android.hardware.Sensor.TYPE_ROTATION_VECTOR;
import static android.hardware.Sensor.TYPE_SIGNIFICANT_MOTION;
import static android.hardware.Sensor.TYPE_STEP_COUNTER;
import static android.hardware.Sensor.TYPE_STEP_DETECTOR;
import static android.hardware.Sensor.TYPE_TEMPERATURE;

/**
 * Created by chris on 4/13/16.
 */
public class SensorUtils {

    public static final String STRING_TYPE_ACCELEROMETER = "android.sensor.accelerometer";
    public static final String STRING_TYPE_MAGNETIC_FIELD = "android.sensor.magnetic_field";
    public static final String STRING_TYPE_GEOMAGNETIC_ROTATION_VECTOR = "android.sensor.geomagnetic_rotation_vector";
    public static final String STRING_TYPE_ORIENTATION = "android.sensor.orientation";
    public static final String STRING_TYPE_GYROSCOPE = "android.sensor.gyroscope";
    public static final String STRING_TYPE_LIGHT = "android.sensor.light";
    public static final String STRING_TYPE_PRESSURE = "android.sensor.pressure";

    public static final String STRING_TYPE_TEMPERATURE = "android.sensor.temperature";
    public static final String STRING_TYPE_AMBIENT_TEMPERATURE = "android.sensor.ambient_temperature";
    public static final String STRING_TYPE_RELATIVE_HUMIDITY = "android.sensor.relative_humidity";
    public static final String STRING_TYPE_PROXIMITY = "android.sensor.proximity";
    public static final String STRING_TYPE_MAGNETIC_FIELD_UNCALIBRATED = "android.sensor.magnetic_field_uncalibrated";
    public static final String STRING_TYPE_GYROSCOPE_UNCALIBRATED = "android.sensor.gyroscope_uncalibrated";
    public static final String STRING_TYPE_ROTATION_VECTOR = "android.sensor.rotation_vector";
    public static final String STRING_TYPE_SIGNIFICANT_MOTION = "android.sensor.significant_motion";

    public static String getStringType(Sensor sensor){
        return getStringType(sensor.getType());
    }

    public static String getStringType(int mType){
        switch (mType) { //TODO: cleanup list, tranfter as much as possible to constants
            case TYPE_ACCELEROMETER:
                return STRING_TYPE_ACCELEROMETER;
            case TYPE_AMBIENT_TEMPERATURE:
                return STRING_TYPE_TEMPERATURE;
            case TYPE_GEOMAGNETIC_ROTATION_VECTOR:
                return STRING_TYPE_GEOMAGNETIC_ROTATION_VECTOR;
            case TYPE_GRAVITY:
                return "android.sensor.gravity";
            case TYPE_GYROSCOPE:
                return STRING_TYPE_GYROSCOPE;
            case TYPE_GYROSCOPE_UNCALIBRATED:
                return STRING_TYPE_GYROSCOPE_UNCALIBRATED;
            case TYPE_LIGHT:
                return STRING_TYPE_LIGHT;
            case TYPE_LINEAR_ACCELERATION:
                return "android.sensor.linear_acceleration";
            case TYPE_MAGNETIC_FIELD:
                return STRING_TYPE_MAGNETIC_FIELD;
            case TYPE_MAGNETIC_FIELD_UNCALIBRATED:
                return "android.sensor.magnetic_field_uncalibrated";
            case 25:
                return "android.sensor.pick_up_gesture";
            case TYPE_PRESSURE:
                return "android.sensor.pressure";
            case TYPE_PROXIMITY:
                return "android.sensor.proximity";
            case TYPE_RELATIVE_HUMIDITY:
                return "android.sensor.relative_humidity";
            case TYPE_ROTATION_VECTOR:
                return "android.sensor.rotation_vector";
            case TYPE_SIGNIFICANT_MOTION:
                return "android.sensor.significant_motion";
            case TYPE_STEP_COUNTER:
                return "android.sensor.step_counter";
            case TYPE_STEP_DETECTOR:
                return "android.sensor.step_detector";
            case 22:
                return "android.sensor.tilt_detector";
            case 23:
                return "android.sensor.wake_gesture";
            case TYPE_ORIENTATION:
                return "android.sensor.orientation";
            case TYPE_TEMPERATURE:
                return "android.sensor.temperature";
            default:
                return "android.sensor.unknown";
        }
    }

    public static int getIntType(String type){
        switch (type) { //TODO: cleanup list, transfer as much as possible to constants
            case STRING_TYPE_ACCELEROMETER:
                return TYPE_ACCELEROMETER;
            case "android.sensor.gravity":
                return TYPE_GRAVITY;
            case STRING_TYPE_GYROSCOPE:
                return TYPE_GYROSCOPE;
            case STRING_TYPE_LIGHT:
                return TYPE_LIGHT;
            case STRING_TYPE_LINEAR_ACCELERATION:
                return TYPE_LINEAR_ACCELERATION;
            case STRING_TYPE_MAGNETIC_FIELD:
                return TYPE_MAGNETIC_FIELD;
            case STRING_TYPE_PRESSURE:
                return TYPE_PRESSURE;
            case STRING_TYPE_PROXIMITY:
                return TYPE_PROXIMITY;
            case STRING_TYPE_ROTATION_VECTOR:
                return TYPE_ROTATION_VECTOR;
            case "android.sensor.tilt_detector":
                return 22;
            case STRING_TYPE_ORIENTATION:
                return TYPE_ORIENTATION;
            case STRING_TYPE_TEMPERATURE:
                return TYPE_TEMPERATURE;
            default:
                return -1;
        }
    }
}
