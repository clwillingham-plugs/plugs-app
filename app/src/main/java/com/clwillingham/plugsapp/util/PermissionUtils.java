package com.clwillingham.plugsapp.util;

import android.content.pm.PackageManager;

/**
 * Created by chris on 3/3/17.
 */

public class PermissionUtils {
    /**
     * Check that all given permissions have been granted by verifying that each entry in the
     */
    public static boolean verifyPermissions(int[] grantResults) {
        // At least one result must be checked.
        if(grantResults.length < 1){
            return false;
        }

        // Verify that each required permission has been granted, otherwise return false.
        for (int result : grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }
}
