package com.clwillingham.plugsapp.util;

import android.util.Log;

import com.clwillingham.plugsapp.events.SensorCalibratedEvent;
import com.clwillingham.plugsapp.sensors.events.BaseSensorEvent;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by chris on 4/24/16.
 */
public class SensorTimestampCalibrator {
    private final float delay;
    private long startTime;
    private long endTime;

    private long divisor = 1; // to get from timestamp to milliseconds
    private long offset = 0; // to get from event milliseconds to system milliseconds

    private long sysTimeMillis1;
    private long sysTimeMillis2;

    private BaseSensorEvent event1;
    private BaseSensorEvent event2;
    private boolean calibrated = false;

    public SensorTimestampCalibrator(float delay){
        this.delay = delay;
        //sensorManager.registerListener(this, sensorManager.getDefaultSensor(type), SensorManager.SENSOR_DELAY_NORMAL);
    }

    //TODO: figure out where this brilliant method came from

    /**
     * Guarantee that sensor time is in Millis since Epoch
     * @param sensorEvent
     */
    public void fixTime(BaseSensorEvent sensorEvent) {
        if(calibrated) {
            sensorEvent.setSensorTimestamp((sensorEvent.getSensorTime() / divisor)+offset);
            return;
        }

        long timestamp = System.currentTimeMillis();

        if(event1 == null){
            event1 = sensorEvent;
            sysTimeMillis1 = timestamp;
            startTime = timestamp;
            endTime = (long)(startTime+2000);
        }else if(event2 == null && System.currentTimeMillis() >= endTime) {
            event2 = sensorEvent;
            sysTimeMillis2 = timestamp;

            long timestampDelta = event2.getSensorTime() - event1.getSensorTime();
            long sysTimeDelta = sysTimeMillis2 - sysTimeMillis1;

            if (timestampDelta / sysTimeDelta > 1000) { // in reality ~1 vs ~1,000,000
                // timestamps are in nanoseconds
                divisor = 1000000;
            } else {
                // timestamps are in milliseconds
                divisor = 1;
            }
            offset = sysTimeMillis1 - (event1.getSensorTime() / divisor);
            calibrated = true;
            EventBus.getDefault().post(new SensorCalibratedEvent(sensorEvent.getSensor(), offset, divisor));
            Log.d("SensorCalibrator", SensorUtils.getStringType(sensorEvent.getSensor().getType()) + " calibrated");
        }
    }

    public long getDivisor() {
        return divisor;
    }

    public long getOffset() {
        return offset;
    }

    public boolean isCalibrated() {
        return calibrated;
    }
}
