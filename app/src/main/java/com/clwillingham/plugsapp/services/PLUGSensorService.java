package com.clwillingham.plugsapp.services;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.clwillingham.plugsapp.App;
import com.clwillingham.plugsapp.R;
import com.clwillingham.plugsapp.actions.Action;
import com.clwillingham.plugsapp.data.ActionEntity;
import com.clwillingham.plugsapp.data.CommandEntity;
import com.clwillingham.plugsapp.data.DeviceEntity;
import com.clwillingham.plugsapp.data.LocationEntity;
import com.clwillingham.plugsapp.data.SensorEntity;
import com.clwillingham.plugsapp.data.TriggerEntity;
import com.clwillingham.plugsapp.events.PLUGServiceStatus;
import com.clwillingham.plugsapp.events.commands.EnableCameraCommand;
import com.clwillingham.plugsapp.events.commands.StopServiceCommand;
import com.clwillingham.plugsapp.events.data.actions.ActionUpdatedEvent;
import com.clwillingham.plugsapp.events.data.commands.CommandCreatedEvent;
import com.clwillingham.plugsapp.events.data.devices.DeviceUpdatedEvent;
import com.clwillingham.plugsapp.events.data.sensors.SensorUpdatedEvent;
import com.clwillingham.plugsapp.events.data.triggers.TriggerCreatedEvent;
import com.clwillingham.plugsapp.events.data.triggers.TriggerDeletedEvent;
import com.clwillingham.plugsapp.events.data.triggers.TriggerUpdatedEvent;
import com.clwillingham.plugsapp.sensors.SensorEventBus;
import com.clwillingham.plugsapp.sensors.events.AccelerometerEvent;
import com.clwillingham.plugsapp.sensors.events.BarometerEvent;
import com.clwillingham.plugsapp.sensors.events.BaseSensorEvent;
import com.clwillingham.plugsapp.sensors.events.GyroEvent;
import com.clwillingham.plugsapp.sensors.events.LightEvent;
import com.clwillingham.plugsapp.sensors.events.MagnetometerEvent;
import com.clwillingham.plugsapp.triggers.Trigger;
import com.clwillingham.plugsapp.util.SensorUtils;
import com.parse.Parse;
import com.parse.ParseException;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class PLUGSensorService extends Service implements SensorEventListener, LocationListener {
    private static final String TAG = "PLUGSensorService";

    private DeviceEntity device;
    public static final int SENSOR_SERVICE_ID = 4564;
    public static final String SENSOR_CHANNEL_ID = "SECRET_SENSOR_STUFF";

    Notification notification;
    private SensorManager sensorManager;
    private LocationManager locationManager;
    private PowerManager powerManager;
    public boolean trackingLocation = false;
    HashMap<String, Trigger> triggers = new HashMap<>();

    public static final int[] SUPPORTED_SENSORS;
    public static final int REQUEST_LOCATION_CODE = 0;

    static {
        //supported sensors of android ICS and up
        SUPPORTED_SENSORS = new int[]{
                Sensor.TYPE_ACCELEROMETER,
                Sensor.TYPE_GYROSCOPE,
                Sensor.TYPE_MAGNETIC_FIELD,
                Sensor.TYPE_PRESSURE,
                Sensor.TYPE_RELATIVE_HUMIDITY,
                Sensor.TYPE_AMBIENT_TEMPERATURE,
                Sensor.TYPE_LIGHT,
                Sensor.TYPE_ROTATION_VECTOR
        };
    }

    private SensorEventBus sensorBus;

    public static Intent createIntent(Context context) {
        return new Intent(context, PLUGSensorService.class);
    }

    App getApp() {
        return (App) getApplication();
    }

    public PLUGSensorService() {

    }

    List<Sensor> getSensorsList() {
        final SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        ArrayList<Sensor> sensors = new ArrayList<>();
        for (int sensorType : SUPPORTED_SENSORS) {
            Log.d("LaunchSetupActivity", SensorUtils.getStringType(sensorType));
            Sensor sensor = sensorManager.getDefaultSensor(sensorType);
            if (sensor != null) {
                sensors.add(sensor);
            }
        }
        return sensors;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        sensorBus = new SensorEventBus(100);
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        CharSequence name = "secret stuff";
        String description = "definitely not spying on you";
        int importance = NotificationManager.IMPORTANCE_LOW;
        NotificationChannel channel = new NotificationChannel(SENSOR_CHANNEL_ID, name, importance);
        channel.setDescription(description);
        // Register the channel with the system; you can't change the importance
        // or other notification behaviors after this
        NotificationManager notificationManager = getSystemService(NotificationManager.class);
        assert notificationManager != null;
        notificationManager.createNotificationChannel(channel);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void setDevice(DeviceEntity entity) throws ParseException {
        device = entity;
        if (device.isDirty()) {

        }
        device.setType("android");
        device.setName("Dev Test 1");
        device.setAsDevice(getApp().getPrefs());
        List<Sensor> sensors = getSensorsList();
        Collection<Action> actions = getApp().getActions();
        Collection<Class<? extends Trigger>> triggerTypes = getApp().getTriggerTypes();
        device.save();
        for (Sensor sensor : sensors) {
            SensorEntity sensorEntity = device.maybeNewSensor(sensor);
            //update the initial state of every existing or new sensor
            EventBus.getDefault().post(new SensorUpdatedEvent(sensorEntity));
        }

        for(Action action : actions){
            device.addAction(action);
        }
        for(Class<? extends Trigger> cls : triggerTypes){
            try {
                device.addTriggerType(cls);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        device.save();

        for(TriggerEntity triggerEntity : device.getTriggers().find()){
            instantiateTrigger(triggerEntity);
        }

//        device.subscribe();
//        device.subscribeToSensors();
//        device.subscribeToCommands();
//        device.subscribeToActions();
        device.subscribeAll();
        getApp().getPrefs().edit().putString(App.PREF_DEVICE_ID, device.getObjectId()).apply();
        updateLocationTracker(device.isTrackingLocation());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void deviceUpdated(DeviceUpdatedEvent e) {
        device = e.getDevice();
        Log.d(TAG, "deviceUpdated tracking location: " + device.isTrackingLocation());
        updateLocationTracker(device.isTrackingLocation());
    }

    public void updateLocationTracker(boolean track) {
        if (track && !trackingLocation) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
            trackingLocation = true;
            Log.d(TAG, "updateLocationTracker: True");
        }else if(!track && trackingLocation){
            locationManager.removeUpdates(this);
            trackingLocation = false;
            Log.d(TAG, "updateLocationTracker: False");
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel();
            notification = new NotificationCompat.Builder(this, SENSOR_CHANNEL_ID)
                    .setContentTitle("Nothing to see here")
                    .setContentText("")
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .build();
            startForeground(SENSOR_SERVICE_ID, notification);
        }
        EventBus.getDefault().register(this);
        DeviceEntity.fetchInBackground(getApp());

        EventBus.getDefault().postSticky(new PLUGServiceStatus(true));


        return START_NOT_STICKY;
    }

    @Subscribe
    public void onStopCommand(StopServiceCommand cmd){
        stopSelf();
    }

    @Subscribe
    public void onSensorEvent(SensorUpdatedEvent event){
        Log.d(TAG, "Sensor UPDATED: " + event.getSensor().getName());
        SensorEntity sensorEntity = event.getSensor();
        int type = event.getSensor().getIntType();
        Sensor sensor = sensorManager.getDefaultSensor(type);
        if(sensorBus.has(type)){
            sensorManager.unregisterListener(this, sensor);
        }
        if(sensorEntity.isEnabled()){
            if(!sensorBus.has(type)){
                sensorBus.addBuffer(type);
            }
            sensorManager.registerListener(this, sensor, sensorEntity.getSampleRate());

        }else{
            sensorBus.removeBuffer(type);
        }
    }

    @Subscribe
    public void onCommandCreated(CommandCreatedEvent event){
        CommandEntity cmd = event.getCommand();
        try {
            cmd.getAction().fetchIfNeeded();
            Log.d(TAG, "onCommandCreated: " + cmd.getAction().getType());
            getApp().getAction(cmd.getAction().getType()).execute(cmd);
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    @Subscribe
    public void onActionUpdatedEvent(ActionUpdatedEvent event){
        ActionEntity entity = event.getAction();
        getApp().getAction(entity.getType()).setEnabled(entity.getEnabled());
    }

    @Subscribe
    public void onBaseSensorEvent(BaseSensorEvent event){
        Log.d(TAG, "onBaseSensorEvent: " + event);
    }

    @Subscribe
    public void enableCamera(EnableCameraCommand cmd){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(CameraService.createIntent(this));
        }else{
            startService(CameraService.createIntent(this));
        }
    }

    @Subscribe
    public void onTriggerCreated(TriggerCreatedEvent triggerCreatedEvent){
        instantiateTrigger(triggerCreatedEvent.trigger);
    }
    @Subscribe
    public void onTriggerUpdated(TriggerUpdatedEvent triggerUpdatedEvent){
        triggers.get(triggerUpdatedEvent.trigger.getObjectId()).setEntity(triggerUpdatedEvent.trigger);
    }
    @Subscribe
    public void onTriggerDeleted(TriggerDeletedEvent triggerDeletedEvent){
        TriggerEntity triggerEntity = triggerDeletedEvent.trigger;
        triggers.get(triggerEntity.getObjectId()).disable();
        triggers.remove(triggerEntity.getObjectId());
    }

    public void instantiateTrigger(TriggerEntity triggerEntity){
        Class<? extends Trigger> triggerCls = getApp().getTriggerType(triggerEntity.getType());
        try {
            Trigger trigger = triggerCls.newInstance();
            trigger.setEntity(triggerEntity);
            triggers.put(triggerEntity.getObjectId(), trigger);
        } catch (IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            stopForeground(STOP_FOREGROUND_REMOVE);
        }
        EventBus.getDefault().unregister(this);
        if(device != null){
            device.unsubscribe();
        }
        EventBus.getDefault().postSticky(new PLUGServiceStatus(true));
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        BaseSensorEvent baseEvent;
        switch (event.sensor.getType()){
            case Sensor.TYPE_ACCELEROMETER:
                baseEvent = new AccelerometerEvent(event);
                break;
            case Sensor.TYPE_GYROSCOPE:
                baseEvent = new GyroEvent(event);
                break;
            case Sensor.TYPE_LINEAR_ACCELERATION:
                baseEvent = new AccelerometerEvent(event);
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                baseEvent = new MagnetometerEvent(event);
                break;
            case Sensor.TYPE_PRESSURE:
                baseEvent = new BarometerEvent(event);
                break;
            case Sensor.TYPE_LIGHT:
                baseEvent = new LightEvent(event);
                Log.d(TAG, "onSensorChanged: Light EVENT: " + Arrays.toString(event.values));
                break;
            default:
                baseEvent = new BaseSensorEvent(event);
                Log.d(TAG, "onSensorChanged: BASE EVENT: " + Arrays.toString(event.values));
        }
//        Log.d(TAG, "onSensorChanged: " + (baseEvent instanceof AccelerometerEvent));
        sensorBus.post(baseEvent);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        //todo: in what situation does this happen with default sensors?
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged: " + location.getLatitude() + ", " + location.getLongitude());
        EventBus.getDefault().postSticky(location);
        LocationEntity locationEntity = new LocationEntity(device, location);
        locationEntity.saveInBackground();

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
