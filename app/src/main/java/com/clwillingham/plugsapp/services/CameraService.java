package com.clwillingham.plugsapp.services;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.androidhiddencamera.CameraConfig;
import com.androidhiddencamera.CameraError;
import com.androidhiddencamera.HiddenCameraService;
import com.androidhiddencamera.HiddenCameraUtils;
import com.androidhiddencamera.config.CameraFacing;
import com.androidhiddencamera.config.CameraImageFormat;
import com.androidhiddencamera.config.CameraResolution;
import com.clwillingham.plugsapp.R;
import com.clwillingham.plugsapp.data.EventEntity;
import com.clwillingham.plugsapp.events.commands.DisableCameraCommand;
import com.clwillingham.plugsapp.events.commands.StopServiceCommand;
import com.clwillingham.plugsapp.events.commands.TakePictureCommand;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.SaveCallback;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class CameraService extends HiddenCameraService {
    private static final String TAG = "CameraService";
    private static final String CAMERA_CHANNEL_ID = "NOT_CAMERA";
    private static final int CAMERA_SERVICE_ID = 2048;
    EventBus bus;
    private Notification notification;
    private EventEntity event;

    public static Intent createIntent(Context context){
        return new Intent(context, CameraService.class);
    }


    @TargetApi(Build.VERSION_CODES.O)
    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        CharSequence name = "not hidden camera";
        String description = "not spying on you";
        int importance = NotificationManager.IMPORTANCE_LOW;
        NotificationChannel channel = new NotificationChannel(CAMERA_CHANNEL_ID, name, importance);
        channel.setDescription(description);
        // Register the channel with the system; you can't change the importance
        // or other notification behaviors after this
        NotificationManager notificationManager = getSystemService(NotificationManager.class);
        assert notificationManager != null;
        notificationManager.createNotificationChannel(channel);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel();
            notification = new NotificationCompat.Builder(this, CAMERA_CHANNEL_ID)
                    .setContentTitle("Nothing to see here")
                    .setContentText("")
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .build();
            startForeground(CAMERA_SERVICE_ID, notification);
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

            if (HiddenCameraUtils.canOverDrawOtherApps(this)) {
                CameraConfig cameraConfig = new CameraConfig()
                        .getBuilder(this)
                        .setCameraFacing(CameraFacing.REAR_FACING_CAMERA)
                        .setCameraResolution(CameraResolution.MEDIUM_RESOLUTION)
                        .setImageFormat(CameraImageFormat.FORMAT_JPEG)
                        .build();

                startCamera(cameraConfig);

                /*new android.os.Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        takePicture();
                    }
                }, 2000);*/
            } else {

                //Open settings to grant permission for "Draw other apps".
                HiddenCameraUtils.openDrawOverPermissionSetting(this);
            }
        } else {
            //TODO Ask your parent activity for providing runtime permission
            Toast.makeText(this, "Camera permission not available", Toast.LENGTH_SHORT).show();
        }
        Log.d(TAG, "onStartCommand: Starting Camera Service");
        return START_NOT_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        bus = EventBus.getDefault();
        bus.register(this);
    }

    @Override
    public void onDestroy() {
        bus.unregister(this);
        Log.d(TAG, "onDestroy: this happened");
        super.onDestroy();
    }

    @Subscribe
    public void onDisableCameraCmd(DisableCameraCommand cmd){
        stopSelf();
    }

    @Subscribe
    public void onStopCommand(StopServiceCommand cmd){
        stopSelf();
    }

    @Subscribe
    public void onPictureCommand(TakePictureCommand cmd){
        takePicture();
        this.event = cmd.getEvent();
        Log.d(TAG, "onPictureCommand: Took a picture");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
//        return null;
        //TODO: maybe return a communication channel here
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onImageCapture(@NonNull File imageFile) {
        BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inPreferredConfig = Bitmap.Config.RGB_565;
//        Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath(), options);
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(imageFile));
            int size = (int) imageFile.length();
            byte[] bytes = new byte[size];
            int result = buf.read(bytes, 0, bytes.length);
            //Do something with the bitmap
            final ParseFile file = new ParseFile("image", bytes);
            file.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    event.setImage(file);
                    event.saveInBackground();
                    Log.d(TAG, "done: " + file.getName());
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.d(TAG, imageFile.getAbsolutePath());
        //stopSelf();
    }

    @Override
    public void onCameraError(@CameraError.CameraErrorCodes int errorCode) {
        switch (errorCode) {
            case CameraError.ERROR_CAMERA_OPEN_FAILED:
                //Camera open failed. Probably because another application
                //is using the camera
                Toast.makeText(this, "Cannot open camera.", Toast.LENGTH_LONG).show();
                break;
            case CameraError.ERROR_IMAGE_WRITE_FAILED:
                //Image write failed. Please check if you have provided WRITE_EXTERNAL_STORAGE permission
                Toast.makeText(this, "Cannot write image captured by camera.", Toast.LENGTH_LONG).show();
                break;
            case CameraError.ERROR_CAMERA_PERMISSION_NOT_AVAILABLE:
                //camera permission is not available
                //Ask for the camera permission before initializing it.
                Toast.makeText(this, "Camera permission not available.", Toast.LENGTH_LONG).show();
                break;
            case CameraError.ERROR_DOES_NOT_HAVE_OVERDRAW_PERMISSION:
                //Display information dialog to the user with steps to grant "Draw over other app"
                //permission for the app.
                HiddenCameraUtils.openDrawOverPermissionSetting(this);
                break;
            case CameraError.ERROR_DOES_NOT_HAVE_FRONT_CAMERA:
                Toast.makeText(this, "Your device does not have front camera.", Toast.LENGTH_LONG).show();
                break;
        }

        stopSelf();
    }
}
