package com.clwillingham.plugsapp.triggers.annotations;

public @interface Arg {
    String key();
    String type();
}
