package com.clwillingham.plugsapp.triggers.annotations;

import org.json.JSONObject;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Args {
    Arg[] value();
}
