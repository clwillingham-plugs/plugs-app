package com.clwillingham.plugsapp.triggers;

import android.util.Log;

import com.clwillingham.plugsapp.sensors.buffers.LightBuffer;
import com.clwillingham.plugsapp.triggers.annotations.Arg;
import com.clwillingham.plugsapp.triggers.annotations.Args;
import com.clwillingham.plugsapp.triggers.annotations.TriggerName;
import com.parse.ParseException;

import org.greenrobot.eventbus.Subscribe;

@TriggerName("Light")
@Args({
        @Arg(key="Sensitivity", type="number")
})
public class LightTrigger extends Trigger {
    private static final String TAG = "LightTrigger";
    @Subscribe
    public void onLightSensor(LightBuffer buffer){
        if(buffer.size() > 2){
            float avg = 0;
            for(int i=1; i < buffer.size()-2; i++){
                avg+= buffer.get(i).getLight();
            }
            avg /= buffer.size()-1;
            Log.d(TAG, "onLightSensor: "+(Math.abs(buffer.get().getLight() - avg)));
            if(Math.abs(buffer.get().getLight() - avg) > 5){
                try {
                    trigger();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
