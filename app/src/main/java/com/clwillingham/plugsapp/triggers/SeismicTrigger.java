package com.clwillingham.plugsapp.triggers;

import android.util.Log;

import com.clwillingham.plugsapp.sensors.buffers.AccelerometerBuffer;
import com.clwillingham.plugsapp.sensors.events.AccelerometerEvent;
import com.clwillingham.plugsapp.triggers.annotations.Arg;
import com.clwillingham.plugsapp.triggers.annotations.Args;
import com.clwillingham.plugsapp.triggers.annotations.Requires;
import com.clwillingham.plugsapp.triggers.annotations.TriggerName;
import com.parse.ParseException;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

@TriggerName("seismic")
@Requires("accelerometer")
@Args({
        @Arg(key = "sensitivity", type = "float")
})
public class SeismicTrigger extends Trigger {
    private static final String TAG = "SeismicTrigger";

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onAcceleration(AccelerometerBuffer buffer) {
        if(!buffer.isFull()) return;
        float[] max = new float[3]; //should initialize at zero
        float[] min = new float[3]; //should initialize at zero
        List<AccelerometerEvent> events = buffer.getAll();
        for(int i=events.size()-2; i > 0; i--){ //get latest event - 1;
            AccelerometerEvent event = events.get(i);
            for(int j=0; j < 3; j++){
                float value = event.getValues()[j];
                if(value > max[j]){
                    max[j] = value;
                }
                if(value < min[j]){
                    min[j] = value;
                }
//                averages[j] += value;
            }
        }
        //iterate through all 3 axis's's's's
        for(int i=0; i < 3; i++){
            //if any new value is outside 20% of the previous value.. panic! (setTriggered to true and return)
            if(buffer.get().getValues()[i] > max[i]*2 || buffer.get().getValues()[i] < min[i]*2){
                try {
                    trigger();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return;
            }
        }
    }
}
