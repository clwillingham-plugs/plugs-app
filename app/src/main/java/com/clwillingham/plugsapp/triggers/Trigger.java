package com.clwillingham.plugsapp.triggers;

import android.util.Log;

import com.clwillingham.plugsapp.data.ActionEntity;
import com.clwillingham.plugsapp.data.EventEntity;
import com.clwillingham.plugsapp.data.TriggerEntity;
import com.clwillingham.plugsapp.events.data.triggers.TriggerUpdatedEvent;
import com.clwillingham.plugsapp.triggers.annotations.TriggerName;
import com.parse.ParseException;
import com.parse.SaveCallback;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.joda.time.DateTime;
import org.json.JSONObject;

import java.util.List;

public class Trigger {
    private static final String TAG = "Trigger";

    protected JSONObject args;
    boolean triggered;
    private DateTime timeout = DateTime.now();
    private EventEntity eventEntity;
    private TriggerEntity entity;
    boolean enabled;

    public void setArgs(JSONObject args){
        this.args = args;
    }

    public void setEntity(TriggerEntity entity){
        this.entity = entity;
        setEnabled(entity.isEnabled());
        setArgs(entity.getArgs());
    }

    public TriggerEntity getEntity() {
        return entity;
    }

    public void setEnabled(boolean enabled){
        if(!enabled && this.enabled){
            disable();
        }else if(enabled && !this.enabled){
            enable();
        }
    }

    public void enable(){
        getEventBus().register(this);
        enabled = true;
        Log.d(TAG, "enable: TRIGGER");
    }

    public void disable(){
        getEventBus().unregister(this);
        enabled = false;
        Log.d(TAG, "disable: TRIGGER");
    }

    public void trigger() throws ParseException {
        Log.d(TAG, "done: TRIGGER");
        if(DateTime.now().isAfter(timeout)){
            Log.d(TAG, "trigger: NEW EVENT");
            eventEntity = new EventEntity();
            eventEntity.setTrigger(entity);
            eventEntity.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if(e != null) {
                        e.printStackTrace();
                        return;
                    }
                    try {
                        eventEntity.fire(entity);
                    } catch (ParseException e1) {
                        e1.printStackTrace();
                    }

                }
            });

        }
        timeout = DateTime.now().plusMillis(1000); //trigger debounce basically
    }

    public EventBus getEventBus(){
        return EventBus.getDefault();
    }

    public String getType() {
        return this.getClass().getAnnotation(TriggerName.class).value();
    }
}
