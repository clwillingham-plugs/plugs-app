package com.clwillingham.plugsapp.sensors.events;

import android.hardware.Sensor;
import android.hardware.SensorEvent;

/**
 * Created by chris on 4/15/16.
 */
public class TemperatureEvent extends BaseSensorEvent {
    public static final int KELVIN = 2;
    public static final int FAHRENHEIT = 1;
    public static final int CELSIUS = 0;


    public TemperatureEvent(SensorEvent event) {
        super(event);
    }

    public TemperatureEvent(long timestamp, Sensor sensor, int accuracy, float[] values) {
        super(timestamp, sensor, accuracy, values);
    }

    public float getTemperature(int unit){
        switch (unit){
            case FAHRENHEIT: return values[0]*(9/5)+32;
            case KELVIN: return values[0]+273.15f;
            default: return values[0];
        }
    }

    public float getTemperature(){
        return values[0];
    }
}
