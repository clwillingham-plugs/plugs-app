package com.clwillingham.plugsapp.sensors.events;

import android.hardware.Sensor;
import android.hardware.SensorEvent;

import org.joda.time.DateTime;

/**
 * Created by chris on 1/21/16.
 */
public class BaseSensorEvent {
    //SensorEvent sensorEvent;
    private long timestamp;
    private Sensor sensor;
    private String type;
    private int accuracy;
    float[] values;

    public BaseSensorEvent(long timestamp, Sensor sensor, int accuracy, float[] values) {
        this.timestamp = timestamp;
        this.sensor = sensor;
        this.accuracy = accuracy;
        this.type = sensor.getName();

        this.values = new float[values.length];//values.clone();
        System.arraycopy(values, 0, this.values, 0, values.length);
    }

    public BaseSensorEvent(SensorEvent event){
        timestamp = event.timestamp;
        sensor = event.sensor;
        accuracy = event.accuracy;
        this.values = new float[event.values.length];//values.clone();
        System.arraycopy(event.values, 0, this.values, 0, event.values.length);
    }

    public long getSensorTime(){
        return timestamp;
    }

    public void setSensorTimestamp(long timestamp){
        this.timestamp = timestamp;
    }

    public DateTime getTimestamp(){
//        return new DateTime((System.nanoTime() - timestamp) / 1000000);
        return new DateTime(timestamp);
    }

    public Sensor getSensor(){
        return sensor;
    }

    public int getAccuracy(){
        return  accuracy;
    }

    public float[] getValues(){
        return values;
    }

    public String getType() {
        return type;
    }
}
