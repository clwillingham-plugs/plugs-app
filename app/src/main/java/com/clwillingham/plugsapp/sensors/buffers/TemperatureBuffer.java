package com.clwillingham.plugsapp.sensors.buffers;

import android.hardware.Sensor;

import com.clwillingham.plugsapp.sensors.SensorBuffer;
import com.clwillingham.plugsapp.sensors.events.MagnetometerEvent;

/**
 * Created by chris on 4/19/16.
 */
public class TemperatureBuffer extends SensorBuffer<MagnetometerEvent> {
    public TemperatureBuffer(int capacity) {
        super(Sensor.TYPE_TEMPERATURE, capacity);
    }
}
