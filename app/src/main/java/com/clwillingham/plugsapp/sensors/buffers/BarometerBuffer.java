package com.clwillingham.plugsapp.sensors.buffers;

import android.hardware.Sensor;

import com.clwillingham.plugsapp.sensors.SensorBuffer;
import com.clwillingham.plugsapp.sensors.events.BarometerEvent;

import java.util.List;

/**
 * Created by chris on 4/19/16.
 */
public class BarometerBuffer extends SensorBuffer<BarometerEvent> {
    public BarometerBuffer(int capacity) {
        super(Sensor.TYPE_PRESSURE, capacity);
    }

    public double getVelocity(){
        List<BarometerEvent> oldHalf = getAll().subList(0, (size()/2)-1);
        List<BarometerEvent> newHalf = getAll().subList((size()/2)+1, size()-1);

        double oldAverageAltitude = getAverageAltitude(1013.25f, oldHalf); //actual ground pressure doesn't matter in this case
        double newAverageAltitude = getAverageAltitude(1013.25f, newHalf); //as we are only comparing the difference between two altitudes

        long oldAverageTime = getAverageTime(oldHalf);
        long newAverageTime = getAverageTime(newHalf);

        return (newAverageAltitude-oldAverageAltitude)/((double)(newAverageTime-oldAverageTime)/1000);
    }

    public double getAveragePressure(){
        return getAveragePressure(getAll());
    }

    private static double getAveragePressure(List<BarometerEvent> events){
        double average = 0;
        for(BarometerEvent event : events){
            average += event.getPressure();
        }
        average /= events.size();
        return average;
    }

    private static double getAverageAltitude(float groundPressure, List<BarometerEvent> events){
        double average = 0;
        for(BarometerEvent event : events){
            average += event.getAltitude(groundPressure);
        }
        average /= events.size();
        return average;
    }

    private static long getAverageTime(List<BarometerEvent> events){
        long averageTime = 0;
        for(BarometerEvent event : events){
            averageTime += event.getSensorTime();
        }
        averageTime /= events.size();
        return averageTime;
    }
}
