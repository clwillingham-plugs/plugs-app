package com.clwillingham.plugsapp.sensors;

import android.hardware.Sensor;
import android.util.SparseArray;

import com.clwillingham.plugsapp.sensors.buffers.AccelerometerBuffer;
import com.clwillingham.plugsapp.sensors.buffers.BarometerBuffer;
import com.clwillingham.plugsapp.sensors.buffers.GyroBuffer;
import com.clwillingham.plugsapp.sensors.buffers.LightBuffer;
import com.clwillingham.plugsapp.sensors.buffers.MagnetometerBuffer;
import com.clwillingham.plugsapp.sensors.buffers.TemperatureBuffer;
import com.clwillingham.plugsapp.sensors.events.BaseSensorEvent;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by chris on 4/17/16.
 */
public class SensorEventBus {
    private SparseArray<SensorBuffer> sensorBufferMap = new SparseArray<>();

    //TODO: find a better solution for storing event handlers
    int defaultCapacity;

    EventBus bus;
    public SensorEventBus(int defaultCapacity){
        this.defaultCapacity = defaultCapacity;
        bus = EventBus.getDefault();
    }

    public void addBuffer(int type, SensorBuffer buffer){
        sensorBufferMap.put(type, buffer);
    }
    public void addBuffer(int type, int capacity){
        SensorBuffer buffer;
        switch (type){
            case Sensor.TYPE_ACCELEROMETER:
                buffer = new AccelerometerBuffer(capacity);break;
            case Sensor.TYPE_PRESSURE:
                buffer = new BarometerBuffer(capacity);break;
            case Sensor.TYPE_GYROSCOPE:
                buffer = new GyroBuffer(capacity);break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                buffer = new MagnetometerBuffer(capacity);break;
            case Sensor.TYPE_AMBIENT_TEMPERATURE:
            case Sensor.TYPE_TEMPERATURE:
                buffer = new TemperatureBuffer(capacity);break;
            case Sensor.TYPE_LIGHT:
                buffer = new LightBuffer(capacity);break;
            default:
                buffer = new SensorBuffer(type, capacity);
        }
        addBuffer(type, buffer);
    }
    public void addBuffer(int type){
        addBuffer(type, defaultCapacity);
    }

    public void removeBuffer(int type){
        sensorBufferMap.delete(type);
    }

    public void post(BaseSensorEvent event){
        int type = event.getSensor().getType();
        SensorBuffer buffer = sensorBufferMap.get(type);
        buffer.add(event);
        bus.postSticky(buffer);
    }

    public SensorBuffer get(int type){
        return sensorBufferMap.get(type);
    }

    public boolean has(int type){
        return get(type) != null;
    }
}
