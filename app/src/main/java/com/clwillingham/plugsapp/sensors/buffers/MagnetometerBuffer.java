package com.clwillingham.plugsapp.sensors.buffers;

import android.hardware.Sensor;

import com.clwillingham.plugsapp.sensors.SensorBuffer;
import com.clwillingham.plugsapp.sensors.events.MagnetometerEvent;

/**
 * Created by chris on 4/19/16.
 */
public class MagnetometerBuffer extends SensorBuffer<MagnetometerEvent> {
    public MagnetometerBuffer(int capacity) {
        super(Sensor.TYPE_MAGNETIC_FIELD, capacity);
    }
}
