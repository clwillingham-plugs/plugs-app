package com.clwillingham.plugsapp.sensors.events;

import android.hardware.Sensor;
import android.hardware.SensorEvent;

/**
 * Created by chris on 4/15/16.
 */
public class AccelerometerEvent extends Vector3SensorEvent {
    public AccelerometerEvent(SensorEvent event) {
        super(event);
    }

    public AccelerometerEvent(long timestamp, Sensor sensor, int accuracy, float[] values) {
        super(timestamp, sensor, accuracy, values);
    }
}
