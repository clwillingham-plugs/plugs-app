package com.clwillingham.plugsapp.sensors.buffers;

import android.hardware.Sensor;

import com.clwillingham.plugsapp.sensors.SensorBuffer;
import com.clwillingham.plugsapp.sensors.events.GyroEvent;

/**
 * Created by chris on 4/19/16.
 */
public class GyroBuffer extends SensorBuffer<GyroEvent> {
    public GyroBuffer(int capacity) {
        super(Sensor.TYPE_GYROSCOPE, capacity);
    }
}
