package com.clwillingham.plugsapp.sensors.events;

import android.hardware.Sensor;
import android.hardware.SensorEvent;

public class LightEvent extends BaseSensorEvent {
    public LightEvent(SensorEvent event) {
        super(event);
    }

    public LightEvent(long timestamp, Sensor sensor, int accuracy, float[] values) {
        super(timestamp, sensor, accuracy, values);
    }
    public float getLight(){
        return getValues()[0];
    }
}
