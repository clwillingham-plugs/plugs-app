package com.clwillingham.plugsapp.sensors;

import android.util.Log;

import com.clwillingham.plugsapp.sensors.events.BaseSensorEvent;
import com.clwillingham.plugsapp.util.SensorTimestampCalibrator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by chris on 10/26/15.
 */
public class SensorBuffer<T extends BaseSensorEvent>  {
    private int capacity;
    private List<T> sensorEvents = new ArrayList<>();
    private int sensorType;
    private boolean isFull = false;
    private SensorTimestampCalibrator calibrator = new SensorTimestampCalibrator(2);

    public SensorBuffer(int type, int capacity){
        this.capacity = capacity;
        sensorType = type;
    }

    public synchronized void add(T event){
        if(isIdentical(event))
            return;
        sensorEvents.add(event);
        calibrator.fixTime(event);
        // Log.d("SensorBuffer", "sensorEvents Size: " + sensorEvents.size());
        if(sensorEvents.size() > capacity){
            sensorEvents.remove(0);
            if(!isFull){
                isFull = true;
                Log.d("SensorBuffer", "Sensor buffer for " + event.getSensor().getName() + " is full");
            }
        }
    }

    private boolean isIdentical(T newEvent){
        if(sensorEvents.isEmpty()){
            return false;
        }
        T lastEvent = get();
        return Arrays.equals(lastEvent.getValues(), newEvent.getValues());
    }

    /**
     * Get latest sensor event
     * @return Latest sensor event
     */
    public T get(){
        return get(0);
    }

    public T getOldest(){
        return sensorEvents.isEmpty()? null : sensorEvents.get(0);
    }

    public T get(int i){
        if(sensorEvents.isEmpty()){
            return null;
        }
        return sensorEvents.get(sensorEvents.size() - 1 - i);
    }

    public int size(){
        return sensorEvents.size();
    }

    public List<T> getAll() {
        return new ArrayList<T>(sensorEvents);
    }

    public int getCapacity() {
        return capacity;
    }
    public int getSensorType(){
        return sensorType;
    }

    public boolean isFull() {
        return isFull;
    }
}
