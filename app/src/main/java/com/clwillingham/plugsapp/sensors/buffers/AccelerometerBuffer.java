package com.clwillingham.plugsapp.sensors.buffers;

import android.hardware.Sensor;

import com.clwillingham.plugsapp.sensors.SensorBuffer;
import com.clwillingham.plugsapp.sensors.events.AccelerometerEvent;

/**
 * Created by chris on 4/19/16.
 */
public class AccelerometerBuffer extends SensorBuffer<AccelerometerEvent> {
    public AccelerometerBuffer(int capacity) {
        super(Sensor.TYPE_ACCELEROMETER, capacity);
    }
}
