package com.clwillingham.plugsapp.sensors.events;

import android.hardware.Sensor;
import android.hardware.SensorEvent;

/**
 * Created by chris on 4/15/16.
 */
public class GyroEvent extends Vector3SensorEvent {
    public GyroEvent(SensorEvent event) {
        super(event);
    }

    public GyroEvent(long timestamp, Sensor sensor, int accuracy, float[] values) {
        super(timestamp, sensor, accuracy, values);
    }
}
