package com.clwillingham.plugsapp.sensors.buffers;

import android.hardware.Sensor;

import com.clwillingham.plugsapp.sensors.SensorBuffer;
import com.clwillingham.plugsapp.sensors.events.LightEvent;

public class LightBuffer extends SensorBuffer<LightEvent> {
    public LightBuffer(int capacity) {
        super(Sensor.TYPE_LIGHT, capacity);
    }
}
