package com.clwillingham.plugsapp.sensors.events;

import android.hardware.Sensor;
import android.hardware.SensorEvent;

/**
 * Created by chris on 1/21/16.
 */
public class Vector3SensorEvent extends BaseSensorEvent{
    public Vector3SensorEvent(SensorEvent event) {
        super(event);
    }

    public Vector3SensorEvent(long timestamp, Sensor sensor, int accuracy, float[] values) {
        super(timestamp, sensor, accuracy, values);
    }

    public float getX(){
        return values[0];
    }
    public float getY(){
        return values[1];
    }
    public float getZ(){
        return values[2];
    }

    public double getMagnitude(){
        return Math.sqrt(Math.pow(getX(), 2) + Math.pow(getY(), 2) + Math.pow(getZ(), 2));
    }
}
