package com.clwillingham.plugsapp.sensors.events;

import android.hardware.Sensor;
import android.hardware.SensorEvent;

/**
 * Created by chris on 1/21/16.
 */
public class BarometerEvent extends BaseSensorEvent{
    public BarometerEvent(SensorEvent event) {
        super(event);
    }

    public BarometerEvent(long timestamp, Sensor sensor, int accuracy, float pressure) {
        super(timestamp, sensor, accuracy, new float[]{pressure});
    }

    /**
     * get the pressure stored in this SensorEvent
     * @return stored pressure in hPA (millibar)
     */
    public float getPressure(){
        return values[0];
    }

    /**
     * get altitude in feet based on stored pressure in SensorEvent
     * @param correction altitude correction, (measured altitude in ft at ground level)
     * @return Altitude in feet
     */
    public double getAltitude(float correction){
        return altitudeCalc(getPressure()) - altitudeCalc(correction);
    }

    private static double altitudeCalc(float value){
        return (1- Math.pow(value/1013.25, 0.190284))*145366.45;
    }
}
