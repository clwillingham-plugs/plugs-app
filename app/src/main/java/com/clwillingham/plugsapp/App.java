package com.clwillingham.plugsapp;

import android.app.Application;
import android.content.SharedPreferences;

import com.clwillingham.plugsapp.actions.Action;
import com.clwillingham.plugsapp.actions.LogAction;
import com.clwillingham.plugsapp.actions.TakePictureAction;
import com.clwillingham.plugsapp.data.ActionEntity;
import com.clwillingham.plugsapp.data.CommandEntity;
import com.clwillingham.plugsapp.data.DeviceEntity;
import com.clwillingham.plugsapp.data.EventEntity;
import com.clwillingham.plugsapp.data.LocationEntity;
import com.clwillingham.plugsapp.data.SensorEntity;
import com.clwillingham.plugsapp.data.TriggerEntity;
import com.clwillingham.plugsapp.triggers.LightTrigger;
import com.clwillingham.plugsapp.triggers.SeismicTrigger;
import com.clwillingham.plugsapp.triggers.Trigger;
import com.clwillingham.plugsapp.triggers.annotations.TriggerName;
import com.parse.Parse;
import com.parse.ParseObject;
import com.parse.livequery.ParseLiveQueryClient;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;
import java.util.prefs.PreferencesFactory;

public class App extends Application {
    public static final String PREF_NAME = "PLUGSApp9000";
    public static final String PREF_DEVICE_ID = "device_id";
    public static final String PREF_APP_ID = "app_id";
    public static final String PREF_CLIENT_KEY = "client_key";
    public static final String PREF_ENDPOINT = "endpoint";
    private static ParseLiveQueryClient client;
    private static HashMap<String, Action> actionMap = new HashMap<>();
    private static HashMap<String, Class<? extends Trigger>> triggerMap = new HashMap<>();
    private boolean parseInitialized = false;

    @Override
    public void onCreate() {
        super.onCreate();
        ParseObject.registerSubclass(DeviceEntity.class);
        ParseObject.registerSubclass(SensorEntity.class);
        ParseObject.registerSubclass(LocationEntity.class);
        ParseObject.registerSubclass(ActionEntity.class);
        ParseObject.registerSubclass(CommandEntity.class);
        ParseObject.registerSubclass(TriggerEntity.class);
        ParseObject.registerSubclass(EventEntity.class);
        initializeParse();
//        Parse.initialize(new  Parse.Configuration.Builder(this)
//                .applicationId("myAppId")
//                .clientKey("myMasterKey")
//                .server("http://192.168.1.248:1337/parse")
////                .server("https://arcane-sea-93348.herokuapp.com/parse")
////                .enableLocalDataStore()
//                .build());

        registerAction(new TakePictureAction());
        registerAction(new LogAction());

        registerTrigger(SeismicTrigger.class);
        registerTrigger(LightTrigger.class);

//        DeviceEntity.fetchInBackground(this);
    }

    public void initializeParse(){
        if (!parseInitialized && getPrefs().contains(PREF_ENDPOINT)) {
            Parse.initialize(new Parse.Configuration.Builder(this)
                    .applicationId(getPrefs().getString(PREF_APP_ID, "plugsApp"))
                    .clientKey(getPrefs().getString(PREF_CLIENT_KEY, "plugsMasterKey"))
                    .server(getPrefs().getString(PREF_ENDPOINT, null))
                    .build());
            parseInitialized = true;
        }
    }

    public boolean isParseInitialized() {
        return parseInitialized;
    }

    private static void registerAction(Action action) {
        actionMap.put(action.getName(), action);
    }

    private static void registerTrigger(Class<? extends Trigger> cls) {
        triggerMap.put(cls.getAnnotation(TriggerName.class).value(), cls);
    }

    public Set<String> getActionNames() {
        return actionMap.keySet();
    }

    public Collection<Action> getActions() {
        return actionMap.values();
    }

    public Action getAction(String action) {
        return actionMap.get(action);
    }

    public Collection<Class<? extends Trigger>> getTriggerTypes() {
        return triggerMap.values();
    }

    public Class<? extends Trigger> getTriggerType(String type) {
        return triggerMap.get(type);
    }

    public static ParseLiveQueryClient getParseClient() {
        if (client != null) {
            return client;
        } else {
            return client = ParseLiveQueryClient.Factory.getClient();
        }

    }

    public SharedPreferences getPrefs() {
        return getSharedPreferences(PREF_NAME, MODE_PRIVATE);
    }
}
