package com.clwillingham.plugsapp.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.clwillingham.plugsapp.App;
import com.clwillingham.plugsapp.R;
import com.parse.Parse;

public class ConfigActivity extends AppCompatActivity implements View.OnClickListener {

    private Button saveBtn;
    private EditText endpointEdtTxt;
    private EditText clientKeyEdtTxt;
    private EditText appIdEdtTxt;

    App getApp(){
        return (App) getApplication();
    }

    public static Intent createIntent(Context context){
        return new Intent(context, ConfigActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);
        saveBtn = findViewById(R.id.saveBtn);
        endpointEdtTxt = findViewById(R.id.endpointEdtTxt);
        clientKeyEdtTxt = findViewById(R.id.clientKeyEdtTxt);
        appIdEdtTxt = findViewById(R.id.appIdEdtTxt);

        endpointEdtTxt.setText(getApp().getPrefs().getString(App.PREF_ENDPOINT, "http://192.168.1.248:1337/parse"));
        clientKeyEdtTxt.setText(getApp().getPrefs().getString(App.PREF_CLIENT_KEY, "plugsMasterKey"));
        appIdEdtTxt.setText(getApp().getPrefs().getString(App.PREF_APP_ID, "plugsApp"));

        saveBtn.setOnClickListener(this);
    }

    private void save(){
        getApp().getPrefs().edit()
                .putString(App.PREF_ENDPOINT, endpointEdtTxt.getText().toString())
                .putString(App.PREF_CLIENT_KEY, clientKeyEdtTxt.getText().toString())
                .putString(App.PREF_APP_ID, appIdEdtTxt.getText().toString())
                .apply();
        if(!getApp().isParseInitialized()){
            getApp().initializeParse();
        }else{
            Toast.makeText(this, "App must be restarted for changes to take affect", Toast.LENGTH_LONG)
                    .show();
        }
        finish();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.saveBtn:
                save();
        }
    }
}
