package com.clwillingham.plugsapp.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.clwillingham.plugsapp.App;
import com.clwillingham.plugsapp.R;
import com.clwillingham.plugsapp.events.PLUGServiceStatus;
import com.clwillingham.plugsapp.events.commands.StopServiceCommand;
import com.clwillingham.plugsapp.services.PLUGSensorService;
import com.clwillingham.plugsapp.util.PermissionUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import static com.clwillingham.plugsapp.services.PLUGSensorService.REQUEST_LOCATION_CODE;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button startBtn;
    private Button settingsBtn;


    App getApp(){
        return (App) getApplication();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CAMERA}, REQUEST_LOCATION_CODE);
        }

        if(!getApp().isParseInitialized()){
            startActivity(ConfigActivity.createIntent(this));
        }

        EventBus.getDefault().register(this);

//        //Check for the camera permission for the runtime
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 101);
//        }


        startBtn = findViewById(R.id.startBtn);
        settingsBtn = findViewById(R.id.settingsBtn);
        startBtn.setOnClickListener(this);
        settingsBtn.setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onStatus(PLUGServiceStatus status){
        if(status.isRunning()){
            startBtn.setText(R.string.stop_text);
        }else{
            startBtn.setText(R.string.start_text);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == REQUEST_LOCATION_CODE && !PermissionUtils.verifyPermissions(grantResults)){
            startBtn.setEnabled(false);
            Toast.makeText(this, "App cannot function properly without location permission", Toast.LENGTH_LONG).show();
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    private void startService(){
//        String endpoint = "http://"+editText.getText().toString()+"/parse";
        Intent intent = PLUGSensorService.createIntent(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent);
        }else{
            startService(intent);
        }
    }

    private void stopService(){
        EventBus.getDefault().post(new StopServiceCommand());
    }

    @Override
    public void onClick(View view) {

        switch(view.getId()){
            case R.id.startBtn:
                if (startBtn.getText().equals(getString(R.string.start_text))) {
                    startService();
//                    startBtn.setText(R.string.stop_text);
                } else {
                    stopService();
//                    startBtn.setText(R.string.start_text);
                }
                break;
            case R.id.settingsBtn:
                startActivity(ConfigActivity.createIntent(this));
                break;

        }
    }
}
