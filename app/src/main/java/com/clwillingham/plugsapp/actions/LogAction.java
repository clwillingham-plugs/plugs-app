package com.clwillingham.plugsapp.actions;

import android.util.Log;

import com.clwillingham.plugsapp.data.CommandEntity;

public class LogAction extends Action {
    private static final String TAG = "LogAction";

    @Override
    public void enable() {

    }

    @Override
    public void execute(CommandEntity cmd) {
        Log.d(TAG, "execute: LOG");
//        try {
//            Log.d(TAG, "execute: " + args.getString("message"));
//        } catch (JSONException | NullPointerException e) {
//            Log.d(TAG, "execute: no message");
//        }
    }

    @Override
    public void disable() {

    }

    @Override
    public String getName() {
        return "log";
    }
}
