package com.clwillingham.plugsapp.actions;

import com.clwillingham.plugsapp.data.CommandEntity;
import com.clwillingham.plugsapp.events.commands.DisableCameraCommand;
import com.clwillingham.plugsapp.events.commands.EnableCameraCommand;
import com.clwillingham.plugsapp.events.commands.TakePictureCommand;

public class TakePictureAction extends Action {

    @Override
    public void enable() {
        getEventBus().post(new EnableCameraCommand());
    }

    @Override
    public void execute(CommandEntity cmd) {
        getEventBus().post(new TakePictureCommand(cmd.getEvent()));
    }

    @Override
    public void disable() {
        getEventBus().post(new DisableCameraCommand());
    }

    @Override
    public String getName() {
        return "picture";
    }
}
