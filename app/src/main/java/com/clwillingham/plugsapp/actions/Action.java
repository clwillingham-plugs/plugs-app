package com.clwillingham.plugsapp.actions;

import android.content.Context;

import com.clwillingham.plugsapp.data.CommandEntity;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

public abstract class Action {

    boolean enabled = false;

    public abstract void enable();
    public abstract void execute(CommandEntity cmd);
    public abstract void disable();

    public abstract String getName();

    public EventBus getEventBus() {
        return EventBus.getDefault();
    }

    public void setEnabled(boolean enabled){
        if(this.enabled && !enabled){
            disable();
        }else if(!this.enabled && enabled){
            enable();
        }
        this.enabled = enabled;
    }

    public static Map<String, Action> getActions(Context context) throws Exception {
        throw new Exception("Not Implemented yet");
    }
}
